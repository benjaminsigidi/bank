require_relative 'lib/bank'
require_relative 'lib/account'

bank = Bank.new('Bank of RubyVillage')
puts "#{ bank.name }"

puts ''
# The code above will be written before Mozzie receives the program.

#Mozzie can edit this section.
#Accounts
keynes = Account.new('Keynes', 'RV001A', 'AN0001')
bank.open_an_account(keynes)

# Transactions
bank.deposit(100.0)
bank.withdraw(25.0)
bank.deposit(10.0)

#And this will also be written before he receives the program.
puts "Current liability, assets and number of accounts."
puts "Liability: #{ bank.liability }"
puts "Assets: #{ bank.assets }"
puts "Number of Accounts: #{ bank.accounts.count}"